package org.flightbooking.controller;

import org.flightbooking.Repository.FlightRepository;
import org.flightbooking.Services.FlightSearchService;
import org.flightbooking.models.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    @Autowired
    private FlightSearchService flightSearchService;

    @RequestMapping("/Hello")
    public String Greeting() {
        return "Hello there on Monday";
    }

    @RequestMapping("/flightobj")
    public Flight flyfunc() {
        Flight f1 = new Flight(1, "hyd", "chennai", "boeing777", 10, 40, 50);
        return f1;
    }

    @RequestMapping("/flightlist")
    public List<Flight> flylist(@RequestParam("FromLoc") String FromLoc,
                                @RequestParam("ToLoc") String ToLoc) {
        String f=FromLoc;
        String t=ToLoc;
        List<Flight> search = flightSearchService.search(f, t, 0);

        return search;
    }

}

