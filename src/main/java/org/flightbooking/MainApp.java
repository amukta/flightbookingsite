package org.flightbooking;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.flightbooking.models.Flight;
import org.flightbooking.Repository.FlightRepository;
import org.springframework.web.bind.annotation.RequestMapping;


@SpringBootApplication
public class MainApp {


        public static void main(String[] args) {

                SpringApplication.run(MainApp.class, args);


        }

}
