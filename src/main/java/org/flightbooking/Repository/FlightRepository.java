package org.flightbooking.Repository;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.flightbooking.models.Flight;
import org.springframework.stereotype.Repository;


@Repository
public class FlightRepository {

    public FlightRepository() {
        getFlightInfo();
    }

    public ArrayList<Flight> getFlightInfo() {
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1, "Bangalore", "Delhi", "Airbus", 12, 38, 50));
        flights.add(new Flight(2, "chennai", "pune", "Boeig777", 10, 40, 50));
        flights.add(new Flight(3, "Bangalore", "Hyderabad", "Airbus", 12, 36, 52));

        return flights;

    }

}