package org.flightbooking.Services;

import org.flightbooking.Repository.FlightRepository;
import org.flightbooking.models.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FlightSearchService {

    @Autowired
    FlightRepository flyrep;

    FlightSearchService(FlightRepository flightRepository) {
        flyrep = flightRepository;
    }


    public List<Flight> search(String source, String destination, int noOfPassengers) {
        if (flyrep.getFlightInfo() == null)
            return new ArrayList<>();

        ArrayList<Flight> al = new ArrayList<>();
        ArrayList<Flight> result = new ArrayList<>();
        al=flyrep.getFlightInfo();
        for(Flight f:al){
            if(f.getFromLoc().equalsIgnoreCase(source) && f.getToLoc().equalsIgnoreCase(destination))
                result.add(f);
        }

        return result;

    }
}

