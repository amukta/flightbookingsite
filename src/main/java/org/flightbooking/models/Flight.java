package org.flightbooking.models;

public class Flight {
    int Id;
    String FromLoc;
    String ToLoc;
    String Model;
    int FcCap;
    int BcCap;
    int EcCap;


    public Flight(int id,String fromLoc, String toLoc, String model, int fcCap, int bcCap, int ecCap) {
        Id=id;
        FromLoc = fromLoc;
        ToLoc = toLoc;
        Model = model;
        FcCap = fcCap;
        BcCap = bcCap;
        EcCap = ecCap;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFromLoc() {
        return FromLoc;
    }

    public void setFromLoc(String fromLoc) {
        FromLoc = fromLoc;
    }

    public String getToLoc() {
        return ToLoc;
    }

    public void setToLoc(String toLoc) {
        ToLoc = toLoc;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public int getFcCap() {
        return FcCap;
    }

    public void setFcCap(int fcCap) {
        FcCap = fcCap;
    }

    public int getBcCap() {
        return BcCap;
    }

    public void setBcCap(int bcCap) {
        BcCap = bcCap;
    }

    public int getEcCap() {
        return EcCap;
    }

    public void setEcCap(int ecCap) {
        EcCap = ecCap;
    }
}
