package org.flightbooking.Services;

import org.flightbooking.Repository.FlightRepository;
import org.flightbooking.models.Flight;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightSearchServiceTest {
    @Mock
    FlightRepository flightRepositoryMock;

    @Test
    public void shouldReturnEmptyListIfAvailableFlightsAreZero() {
        when(flightRepositoryMock.getFlightInfo()).thenReturn(new ArrayList<>());

        FlightSearchService flightSearchService = new FlightSearchService(flightRepositoryMock);
        List<Flight> flights = flightSearchService.search("sou", "hyd", 0);
        assertEquals(0, flights.size());
    }
    @Test
    public void shouldReturnListOfFlightsMatchingCriteria(){

        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(1, "Bangalore", "Delhi", "Airbus", 12, 38, 50));
        flights.add(new Flight(2, "chennai", "pune", "Boeig777", 10, 40, 50));
        flights.add(new Flight(3, "Bangalore", "Hyderabad", "Airbus", 12, 36, 52));

        when(flightRepositoryMock.getFlightInfo()).thenReturn(flights);
        FlightSearchService flightSearchService = new FlightSearchService(flightRepositoryMock);
        List<Flight> flights2 = flightSearchService.search("Bangalore","Delhi",1);
        assertEquals(1, flights2.size());
    }

}